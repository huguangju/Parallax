(function(){
    var Home = function (){
        this.init();
        this.pIndex = 0;
    }

    Home.prototype = {
        init: function () {
            this.paceLoader();
            this.navScroll();
            this.showIndex();
            this.scrollPage();
            this.sideNav();
            this.tab();
            this.slide('mpcA');
            this.slide('mpcS');
            this.keyNav();
            this.showWeiXin();
            this.lazylinepainter();
        },

        paceLoader: function(){
            console.time("加载时间");
            window.paceOptions = {
                elements: true
            };

            Pace.on('hide', function(){
                console.timeEnd("加载时间");
            });
        },

        /**
         * 导航点击定位
         */
        navScroll: function () {
            var $ele = $('#home-nav').find('a');
            $ele.on('click', function () {
                var $this = $(this);
                $this.siblings('a').removeClass('current');
                $this.addClass('current');
            });
        },

        /**
         * 当前滚动高度，返回索引值
         */
        showIndex: function () {
            $(window).bind('scroll',function(e){
                var _mIndex = $('#u-index').offset().top - 60,
                    _mIntro = $('#u-intro').offset().top - 60,
                    _mService = $('#u-service').offset().top - 60,
                    _mPlatform = $('#u-platform').offset().top - 60,
                    _mResearch = $('#u-research').offset().top - 60,
                    _mResolve = $('#u-resolve').offset().top - 60,
                    _mCase = $('#u-case').offset().top - 60;
                    //_mPartners = $('#u-partners').offset().top - 60;
                var $doc = $(document);
                if($doc.scrollTop() >= _mIndex && $doc.scrollTop() < _mIntro){
                    $('#homebody').attr('data-index', 0);
                    $('#home-nav').find('a').removeClass('current').eq(0).addClass('current');
                } else if ($doc.scrollTop() >= _mIntro && $doc.scrollTop() < _mService) {
                    $('#homebody').attr('data-index', 1);
                    $('#home-nav').find('a').removeClass('current').eq(1).addClass('current');
                } else if ($doc.scrollTop() >= _mService && $doc.scrollTop() < _mPlatform) {
                    $('#homebody').attr('data-index', 2);
                    $('#home-nav').find('a').removeClass('current').eq(2).addClass('current');
                } else if ($doc.scrollTop() >= _mPlatform && $doc.scrollTop() < _mResearch) {
                    $('#homebody').attr('data-index', 3);
                    $('#home-nav').find('a').removeClass('current').eq(3).addClass('current');
                } else if ($doc.scrollTop() >= _mResearch && $doc.scrollTop() < _mResolve) {
                    $('#homebody').attr('data-index', 4);
                    $('#home-nav').find('a').removeClass('current').eq(4).addClass('current');
                } else if ($doc.scrollTop() >= _mResolve && $doc.scrollTop() < _mCase) {
                    $('#homebody').attr('data-index', 5);
                    $('#home-nav').find('a').removeClass('current').eq(5).addClass('current');
                } else if ($doc.scrollTop() >= _mCase ) { //&& $doc.scrollTop() < _mPartners
                    $('#homebody').attr('data-index', 6);
                    $('#home-nav').find('a').removeClass('current').eq(6).addClass('current');
                }/*   else if ($doc.scrollTop() >= _mPartners) {
                    $('#homebody').attr('data-index', 7);
                    $('#home-nav').find('a').removeClass('current').eq(7).addClass('current');
                }*/


            });
        },

        /**
         * 滑动页面
         */
        scrollPage: function () {
            var $ele = $('#home-nav').find('a');
            $ele.on('click', function () {
                var _id = $(this).attr('data-id'),
                    _st = ($('#'+_id).offset().top - 60)+ 'px';
                $('html,body').animate({scrollTop: _st}, 400);
            });
        },

        /**
         * 侧栏滚动
         */
        sideNav: function () {
            var $sn = $('#side-nav');
            $sn.find('a').on('click', function (event){
                var $this = $(this),
                    _type = $this.attr('data-type'),
                    $hb = $('#homebody'),
                    $html = $('html,body'),
                    _index = parseInt($hb.attr('data-index'));
                switch (_type) {
                    case 'top':
                        $html.animate({scrollTop: '0'}, 400);
                        $hb.attr('data-index', 0);
                        break;
                    case 'prev':
                        if (_index > 0) {
                            var _bst = ($('.u-block').eq(_index-1).offset().top - 60)+'px';
                            $html.animate({scrollTop: _bst}, 400);
                            $hb.attr('data-index', _index-1);
                        }
                        break;
                    case 'next':
                        if (_index < 6 ) {
                            var _bst = ($('.u-block').eq(_index+1).offset().top - 60)+'px';
                            $html.animate({scrollTop: _bst}, 400);
                            $hb.attr('data-index', _index+1);
                        }
                        break;
                    case 'share':
                        $('#side-share-box').show();
                        event.stopPropagation();
                        break;
                }
            });

            $('.s-restart').on('click', function(){
                $('html,body').animate({scrollTop: '0'}, 400);
                $('#homebody').attr('data-index', 0);
            });

        },

        lazylinepainter: function(){
            var svgData = {
                "ll-horizontal-grid" : { "strokepath" : [{"path": "M 2 424.5 L 579 424.5","duration":400},{"path": "M 2 404.5 L 579 404.5","duration":400},{"path": "M 2 384.5 L 579 384.5","duration":300},{"path": "M 2 364.5 L 579 364.5","duration":300},{"path": "M 2 344.5 L 579 344.5","duration":300},{"path": "M 2 324.5 L 579 324.5","duration":300},{"path": "M 2 304.5 L 579 304.5","duration":200},{"path": "M 2 284.5 L 579 284.5","duration":200},{"path": "M 2 264.5 L 579 264.5","duration":200},{"path": "M 2 244.5 L 579 244.5","duration":200},{"path": "M 2 224.5 L 579 224.5","duration":200},{"path": "M 2 204.5 L 579 204.5","duration":100},{"path": "M 2 184.5 L 579 184.5","duration":100},{"path": "M 2 164.5 L 579 164.5","duration":100},{"path": "M 2 144.5 L 579 144.5","duration":100},{"path": "M 2 124.5 L 579 124.5","duration":100},{"path": "M 2 103.5 L 579 103.5","duration":100},{"path": "M 2 83.5 L 579 83.5","duration":100},{"path": "M 2 63.5 L 579 63.5","duration":50},{"path": "M 2 43.5 L 579 43.5","duration":50},{"path": "M 2 23.5 L 579 23.5","duration":50}] , "dimensions" : { "width" :  582, "height" : 448} },
                "ll-vertical-grid" : { "strokepath" : [{"path": "M 29.5 4 L 29.5 443","duration":400},{"path": "M 49.5 4 L 49.5 443","duration":400},{"path": "M 69.5 4 L 69.5 443","duration":300},{"path": "M 89.5 4 L 89.5 443","duration":300},{"path": "M 109.5 4 L 109.5 443","duration":300},{"path": "M 129.5 4 L 129.5 443","duration":300},{"path": "M 149.5 4 L 149.5 443","duration":300},{"path": "M 169.5 4 L 169.5 443","duration":300},{"path": "M 189.5 4 L 189.5 443","duration":200},{"path": "M 209.5 4 L 209.5 443","duration":200},{"path": "M 229.5 4 L 229.5 443","duration":200},{"path": "M 249.5 4 L 249.5 443","duration":200},{"path": "M 269.5 4 L 269.5 443","duration":200},{"path": "M 289.5 4 L 289.5 443","duration":200},{"path": "M 309.5 4 L 309.5 443","duration":100},{"path": "M 329.5 4 L 329.5 443","duration":100},{"path": "M 349.5 4 L 349.5 443","duration":100},{"path": "M 369.5 4 L 369.5 443","duration":100},{"path": "M 389.5 4 L 389.5 443","duration":100},{"path": "M 409.5 4 L 409.5 443","duration":100},{"path": "M 429.5 4 L 429.5 443","duration":100},{"path": "M 449.5 4 L 449.5 443","duration":100},{"path": "M 469.5 4 L 469.5 443","duration":50},{"path": "M 489.5 4 L 489.5 443","duration":50},{"path": "M 510.5 4 L 510.5 443","duration":50},{"path": "M 530.5 4 L 530.5 443","duration":50},{"path": "M 550.5 4 L 550.5 443","duration":50}] , "dimensions" : { "width" :  582, "height" : 448} },
                "lazyline" : { "strokepath" : [{"path": "M 229 385 L 271 345","duration":200},{"path": "M 309 346 L 349 386","duration":200},{"path": "M 409 244 L 450 285","duration":200},{"path": "M 411 205 L 450 166","duration":200},{"path": "M 128 283 L 170 243","duration":200},{"path": "M 129 163 L 171.5 205.5","duration":200},{"path": "M 390 326 L 369.5 305.5","duration":200},{"path": "M 189 325 L 209 305","duration":200},{"path": "M 189 286 L 210.073 264.927","duration":200},{"path": "M 230 326 L 250.91 305.09","duration":200},{"path": "M 329.206 307.207 L 351 328","duration":200},{"path": "M 370.293 267.29 L 390.001 285.999","duration":200},{"path": "M 190 164 L 208 182","duration":200},{"path": "M 190 124 L 208.5 142.5","duration":200},{"path": "M 229 122 L 250 143","duration":200},{"path": "M 229.5 63 L 270.25 103.75","duration":200},{"path": "M 310 104 L 349.5 64.5","duration":200},{"path": "M 390 126 L 371 145","duration":200},{"path": "M 329.509 144.509 L 350.509 123.509","duration":200},{"path": "M 372.502 182.502 L 392.002 164.002","duration":200},{"path": "M 289.749,63.41 L 309,84 309,103 329,103   350,82 350,125 390,125 390,164 431,164 409,185.5 409,204 429,204 449.167,224.333 428.667,245 408,245 408,262 431.334,285   390,285 390,326 351,326 351,367 329,345 310,345 310,364 290.333,383.666 271,364.333 271,344 252,344 230,366 230,326 190,326   190,286 150,286 170,265.333 170,243 149,243 131.833,224.667 152.333,206 173,206 173,184 152.833,164 190,164 190,124 230,124   230,82 252.667,105 270,105 270,83 289.167,64.833 ","duration":4000},{"path": "M 229,43.83 L 330.167,145 371,145   372,183.333 470.834,285.167 ","duration":400},{"path": "M 347.67,405.24 L 249.666,306 209,306   209,265 107,165 89,183 131.667,225.667 92.333,264.834 108.666,282 128,282 128,306 169,306 169,325 190,325 190,347 209,347   209,386 228,386 228,406 248.333,426.333 290.666,384 330.666,424 349,404.666 349,386 368,386 368,344 389,344 389,324 408,324   408,305 450,305 450,285 470,285 488.166,266.834 447.333,226.001 488.333,185.167 469.999,167 450,167 450,145 408,145 408,126   390,126 390,104 368,104 368,64 348,64 348,43 330.833,25.833 291.666,65 250.333,23.667 230,44 230,62 208,62 208,105 190,105   190,124 166,124 166,144 129,144 129,163 108,163 ","duration":4000},{"path": "M 107.834,282.33 L 208,182.334 208,143   250,143 348.5,44.5 ","duration":400},{"path": "M 228.833,405. L 328,306 371,306 371,268   471.667,167.333 ","duration":400},{"path": "M 349.667,244.33 L 309.667,204.333 270,244   309,244 269.167,204 229.083,244.083 ","duration":1000}] , "dimensions" : { "width" :  582, "height" : 448} }
            }

            $(document).ready(function(){

                // Setup your Lazy Line element.
                // see README file for more settings
                $('#lazyline').lazylinepainter({
                        'svgData' : svgData,
                        'strokeWidth':1,
                        'strokeColor':'#849294',
                        'onComplete' : function(){
                            $(this).animate({'marginTop':60},500);
                        }
                    }
                ).lazylinepainter('p');

                // Paint your Lazy Line, that easy!
                $('#lazyline').lazylinepainter('paint');

                $('#ll-horizontal-grid').lazylinepainter({
                        'svgData' : svgData,
                        'strokeWidth':1,
                        'strokeColor':'#dbd9d9'
                    }
                ).lazylinepainter('paint');

                $('#ll-vertical-grid').lazylinepainter({
                        'svgData' : svgData,
                        'strokeWidth':1,
                        'strokeColor':'#dbd9d9'
                    }
                ).lazylinepainter('paint');
            })
        },

        /**
         * 切换合作伙伴
         */
        tab: function () {
            $('#mpaNav').find('a').on('click', function(){
                var _index = $(this).index();
                $('#mpaNav').removeClass('mn-0 mn-1 mn-2 mn-3').addClass('mn-'+_index);
                $('.mpa-bs').hide().eq(_index).show();
            });
        },

        /**
         * 幻灯切换
         */
        slide: function (eleId) {
            var $id = $('#'+eleId),
                $li = $id.find('.adt-slide').find('li'),
                len = $li.length,
                index = 0,
                _htmlNum = '';

            $li.eq(0).show();

            (function(){
                for(var i = 0; i < len; i++){
                    if (i == 0) {
                        _htmlNum += '<a href="javascript:;" class="current"></a>';
                    } else {
                        _htmlNum += '<a href="javascript:;"></a>';
                    }
                }
            })();

            $id.find('.mpa-pointer').append(_htmlNum);

            $id.find('.mpa-pointer').find('a').mouseover(function(){
                index = $('#'+eleId).find('.mpa-pointer').find('a').index(this);
                picShow(index);
            });

            $id.hover(function(){
                if(playPic){
                    clearInterval(playPic);
                }
            },function(){
                playPic = setInterval(function(){
                    picShow(index);
                    index++;
                    if(index==len){index=0}
                },3000)
            })
            var playPic = setInterval(function(){
                picShow(index);
                index++;
                if(index==len){index=0}
            },3000)
            function picShow(i){
                $li.eq(i).stop(true,true).fadeIn().siblings().fadeOut();
                $('#'+eleId).find('.mpa-pointer').find('a').eq(i).addClass('current').siblings().removeClass('current');
            }
        },

        /**
         * 键盘导航
         */
        keyNav: function () {

            $(document).on('keydown',function(event) {

                if (event == undefined) {
                    event = window.event;
                }

                var _index = parseInt($('#homebody').attr('data-index'));
                if ( ( event.keyCode == 40 || event.keyCode == 34 ) && _index < ($('#home-main > div').length -1 ) && !$('html,body').is(":animated")) {
                    var _bst;
                    try{
                        _bst = ($('.u-block').eq(_index+1).offset().top - 60)+'px';
                    }catch(e){}

                    event.preventDefault();
                    // $(window)._scrollable().stop();
                    $('html,body').stop(true,true).animate({ scrollTop: _bst }, 400);
                    $('#homebody').attr('data-index', _index+1);
                    console.log('index:',_index)
                } else if ( (event.keyCode == 38 || event.keyCode == 33 ) && _index != 0 && !$('html,body').is(":animated")) {
                    if(_index <= 6){
                        var _bst = ($('.u-block').eq(_index-1).offset().top - 60)+'px';
                        event.preventDefault();
                        $('html,body').stop(true,true).animate({ scrollTop: _bst }, 400);
                        $('#homebody').attr('data-index', _index-1);
                    }
                }
            });

        },

        showWeiXin: function (){
            var $t = $('.s-weixin');

            $t.on('click', function(event){
                var $this = $(this),
                    _img = $this.attr('data-img-url'),
                    _left = ($this.offset().left -100 + 29 ) +'px',
                    _top = ($this.offset().top - 220) + 'px';
                var _weixin = '<div id="weixin-QC" style="position: absolute;text-align: center;width: 200px; height: 212px"><img src="'+_img+'" alt="微信"/></div>';
                if ($('#weixin-QC').length) {
                    $('#weixin-QC').show();
                } else {
                    $('body').append(_weixin);
                    $('#weixin-QC').css({'top': _top, 'left': _left});
                }

                event.stopPropagation();

            });

            $('body').on('click', function(){
                $('#weixin-QC').hide();
                $('#side-share-box').hide();
            });
        }

    }

    new Home();
})();

/**
 * loading
 */
$(window).load(function(){
    $('#loading').fadeOut(600, function(){
        $("#index-content").fadeIn(1000);
    });
});


$(function(){
    $('#mi-text').parallax("100%", 0-0.5);
    $('#mi-sq').parallax("50%", 0-0.7);
    $('#bubbles').parallax("20%",0.35);
    $('#mp-cloud').parallax("50%", 0-0.7);
    $('#mp-logo').parallax("50%", 0.35);
    $('#mre-cloud').parallax("50%", 0.3);
    $('#mre-ufo').parallax("50%", 0.5);
    $('#mres-gar').parallax("50%", 0.7);
    $('#mres-logo').parallax("50%", 0-0.5);
    $('#mres-glass').parallax("50%", 0.5);
    $('#geometry5').parallax("1%",0.5)
})